import { Form, Button } from 'react-bootstrap'
import { useEffect, useState, useContext } from 'react'
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';


export default function Login() {
  // allow us to consume the User Context object and its properties 

  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [isActive, setIsActive] = useState(false);

  console.log(email);
  console.log(password);


  useEffect(() => {

    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);


  function loginUser(event) {
    event.preventDefault();

    localStorage.setItem('email', email);

    setUser({
      email: localStorage.getItem('email')
    })

    setEmail('');
    setPassword('');
    alert("Login Successfull!");

  }


  return (
    (user.email !== null) ?
      <Navigate to="/courses" />
      :

      <Form onSubmit={(event) => loginUser(event)}>
        <Form.Group className="mb-3" controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control type="email" placeholder="Enter email" value={email} onChange={event => setEmail(event.target.value)} required />
        </Form.Group>
        <Form.Group className="mb-3" controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Password" value={password} onChange={event => setPassword(event.target.value)} required />
        </Form.Group>

        {/* conditional rendering -> if active button is clickable -> if inactive button is not clickable */}
        {
          (isActive) ?
            <Button variant="primary" type="login" controlId="submitBtn" className="btn btn-success">
              login
            </Button>
            :
            <Button variant="primary" type="login" controlId="submitBtn" className="btn btn-success" disabled>
              login
            </Button>

        }

      </Form>

  )
}