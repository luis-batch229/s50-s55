import Banner from '../components/Banner.js';
import Highligths from '../components/Highlights.js'

export default function Home() {
	return(
		<>
			<Banner/>
	      	<Highligths/>
      	</>
		)
}